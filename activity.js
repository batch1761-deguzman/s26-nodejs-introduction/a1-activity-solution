let http = require("http");
console.log(http);

http.createServer(function(req,res){

	console.log(req.url);

	// response.writeHead(200,{'Content-Type':'text/plain'});
	// response.end("Test Message");
	if(req.url === "/"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("Welcome to B176 Booking System");
	} else if (req.url === "/courses"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("Welcome to the Courses Page. View our Courses")
	} else if (req.url === "/profile"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("Welcome to your profile. View your details");
	} else{
		res.writeHead(404,{'Content-Type':'text/plain'});
		res.end("Resource not Found");
	}

}).listen(5000);

console.log("Server is running on localhost:5000")
